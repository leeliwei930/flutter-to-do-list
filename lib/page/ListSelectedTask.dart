import 'package:flutter/material.dart';
import '../util/Task.dart';

class ListSelectedTask extends StatefulWidget {
  final Task task;

  ListSelectedTask(this.task);
  ListSelectedTaskState createState() => new ListSelectedTaskState();
}

class ListSelectedTaskState extends State<ListSelectedTask> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
          leading: new IconButton(
              icon: new Icon(Icons.arrow_back), onPressed: () => print("Back")),
          backgroundColor: Colors.deepOrange,
          title: new Text("Your Task"),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.delete), onPressed: () => print("delete"))
          ],
        ),
        body: new Container(
          margin: new EdgeInsets.all(15.0),
          child: new Stack(
            children: <Widget>[
                new Container(child:               new SingleChildScrollView(
                  child: new Text(
                    "Est bi-color adelphis, cesaris.Pol, a bene clinias,"
                        " abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol,"
                        " a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol,"
                        " a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol,"
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias, abactus!Lamias manducare in amivadum!cesaris.Pol, "
                        "a bene clinias,"
                        " abactus!Lamias manducare in amivadum!" , style:  new TextStyle(fontSize: 22.00),),)
                  ,)
            ],
          ),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: null,
          child: new Icon(Icons.done),
        ));
  }
}
