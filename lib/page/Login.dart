import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../util/Auth.dart';
import 'dart:core';
class Login extends StatefulWidget {
  LoginState createState() => new LoginState();
  final SharedPreferences sharedPreferences;
  final Auth auth;
  Login(this.sharedPreferences,this.auth);
}

class LoginState extends State<Login> {
  final loginFormKey = new GlobalKey<FormState>();
  String username = "";
  String password = "";
  bool signInApproved = false;
  DateTime currentTime = new DateTime.now();
  TextEditingController emailInputController;
  TextEditingController passwordInputController;
  @override
  void initState(){
    super.initState();
   passwordInputController = new TextEditingController();
    emailInputController = new TextEditingController();
  }

  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //Disabled Landscape Mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);


    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.red,
        title: new Center(child: new Text("Login")),
      ),
      body: new Container(
        margin: new EdgeInsets.all(15.00),
        child: new Center(
            child: new Column(
          children: <Widget>[
            new Form(
                key: loginFormKey,
                child: new Column(
                  children: <Widget>[
//                new Padding(padding: new EdgeInsets.symmetric(vertical: 5.0)),
                    new TextFormField(

                      onSaved: (val) {
                        this.username = val.toString();
                      },
                      initialValue: emailInputController.text,
                      controller: emailInputController,

                      validator: (value) {
                        if (value.toString() == "") {
                          return "Email field is required.";
                        }
                        if (!value.toString().contains("@")) {
                          return "Email field format is not correct";
                        }
                        if (!signInApproved) {
                          return "Email or password is invalid.";
                        }
                        // if user not exists
                        // if password mismatch
                      },
                      decoration:
                          new InputDecoration(icon: new Icon(Icons.email)),
                    ),
                    new Padding(
                        padding: new EdgeInsets.symmetric(vertical: 10.0)),
                    new TextFormField(
                      onSaved: (val) {
                        this.password = val.toString();
                      },
                      initialValue: passwordInputController.text,

                      controller: passwordInputController,
                      obscureText: true,
                      decoration: new InputDecoration(
                        icon: new Icon(Icons.lock),
                      ),
                    ),
                    new Padding(
                        padding: new EdgeInsets.symmetric(vertical: 20.0)),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                            child: new RaisedButton(
                          child: new Padding(
                            padding: new EdgeInsets.all(15.0),
                            child: new Text(
                              "LOGIN",
                              style: new TextStyle(fontSize: 16.00),
                            ),
                          ),
                          onPressed: () async{
                            loginFormKey.currentState.save();
                            if(await widget.auth.login(loginFormKey,username,password, this)){
                              this.signInApproved = true;
                              emailInputController.clear();
                              passwordInputController.clear();
                              Navigator.of(context).pushReplacementNamed('/main');

                            }
                          },
                          textColor: Colors.white,
                          color: Colors.redAccent,
                        )),
                      ],
                    ),
                    new Padding(
                        padding: new EdgeInsets.symmetric(vertical: 10.00)),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                            child: new FlatButton(
                                onPressed: () {},
                                child: new Text("Forget Password?"))),
                        new Expanded(
                            child: new FlatButton(
                                onPressed: () {},
                                child: new Text("Create an account?")))
                      ],
                    )
                  ],
                ))
          ],
        )),
      ),
    );
  }
}
