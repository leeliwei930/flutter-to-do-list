import 'package:flutter/material.dart';
import '../util/Task.dart';
import 'package:event_bus/event_bus.dart';
class CreateTaskField extends StatefulWidget {
 static CreateTaskFieldState of(BuildContext context) => context.ancestorStateOfType(const TypeMatcher<CreateTaskFieldState>());

  CreateTaskFieldState  createState() => new CreateTaskFieldState();
 final Task task;
 final EventBus event = new EventBus();
  CreateTaskField(this.task);


}


class CreateTaskFieldState extends State<CreateTaskField> {
  final formKey = new GlobalKey<FormState>();
  String taskNameField  = "";
  String taskDescriptionField = "";
  @override
  void initState(){
    super.initState();
    taskNameFieldController = new TextEditingController();
    taskDescriptionFieldController = new TextEditingController();
  }

  void validateForm()async{


    final form = formKey.currentState;
      if(form.validate()){
        form.save();
        taskNameFieldController.clear();
        taskDescriptionFieldController.clear();
          await widget.task.createTask(
              taskNameField.toString(), taskDescriptionField.toString() , this);
          Navigator.pop(context);





      }
  }
  TextEditingController taskNameFieldController ;
  TextEditingController taskDescriptionFieldController;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    return  new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.deepOrange,
        title: new Text("Create A New Task"),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context) ),
      ),
      body:   new Stack(

          children:<Widget>[
            new Container(
              child: new  SingleChildScrollView(

                  child: new Container(

                    padding: new EdgeInsets.all(12.0),
                    child: new Form(
                        key: formKey,
                        child: new Column(
                          children: <Widget>[
                            new TextFormField(
                              controller: taskNameFieldController,

                              decoration: new InputDecoration(labelText: 'Task Name'),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return "Task field shouldn't be empty.";
                                }
                                if (val.length >= 20) {
                                  return "Task field characters length shoud not be exceed 20 characters.";
                                }
                              },
                              onSaved: (val){
                                this.taskNameField = val.toString();
                              },
                            ),
                            new TextFormField(
                              controller: taskDescriptionFieldController,
                              maxLines: 6,
                              decoration: new InputDecoration(labelText: 'Task Description'),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return "Task field shouldn't be empty.";
                                }
                                if (val.length >= 50) {
                                  return "Task field characters length shoud not be exceed 50 characters.";
                                }
                              },
                              onSaved: (val){
                                this.taskDescriptionField = val.toString();
                              },
                            ),
                            new Padding(padding: new EdgeInsets.only(top: 20.0)),
                          ],
                        )),

                  )),

            ),

          ]
      ),
      floatingActionButton: new FloatingActionButton(onPressed: ()=> validateForm() , child: new Icon(Icons.add), backgroundColor: Colors.teal,),

    );
  }
}
