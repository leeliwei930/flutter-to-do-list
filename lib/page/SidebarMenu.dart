import 'package:flutter/material.dart';
import '../util/Auth.dart';
class SidebarMenu extends StatefulWidget {
  final Auth auth;
  final BuildContext scaffoldContext;
  final state;
  SidebarMenu(this.auth , this.scaffoldContext,this.state);
  SidebarMenuState createState() => new SidebarMenuState();
}

class SidebarMenuState extends State<SidebarMenu> {

  void initState(){
    super.initState();

    // TODO: implement initState
  }
  @override

  Widget build(BuildContext context) {
    // TODO: implement build

    return new Drawer(
        child: new Padding(
          padding: new EdgeInsets.all(8.00),
          child: new Column(
            children: <Widget>[
              new ListTile(
                  title: new Padding(
                    padding: new EdgeInsetsDirectional.only(top: 50.0),
                    child: new Text(widget.auth.getCurrentUserName().toString()),
                  ),
                  subtitle: new Padding(
                    padding: new EdgeInsetsDirectional.only(
                        top: 14.00, bottom: 50.0),
                    child: new Text(widget.auth.getCurrentUserEmail().toString()),
                  )),
              new ListBody(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Icon(Icons.person),
                        new Padding(padding: new EdgeInsets.all(5.0)),
                        new Text("My Account")
                      ],
                    ),
                    onTap: () {},
                  ),
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Icon(Icons.exit_to_app),
                        new Padding(padding: new EdgeInsets.all(5.0)),
                        new Text("Logout")
                      ],
                    ),
                    onTap: ()async {
                     if( await widget.auth.logout(widget.scaffoldContext,widget.auth)){
                       widget.state.setState((){
                         Navigator.pushReplacementNamed(context, "/login");

                       });


                     }
                    },
                  )
                ],
              )
            ],
          ),
        ));
  }
}