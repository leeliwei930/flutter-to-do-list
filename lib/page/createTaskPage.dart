import 'package:flutter/material.dart';
import './createTaskField.dart';
import '../util/Task.dart';
class CreateTaskPage extends StatefulWidget {
  final Task task;
  CreateTaskPage(this.task);
  CreateTaskPageState createState() => new CreateTaskPageState();

}

class CreateTaskPageState extends State<CreateTaskPage> {

  @override

  Widget build(BuildContext context) {

    // TODO: implement build
    return new CreateTaskField(widget.task);
  }
}