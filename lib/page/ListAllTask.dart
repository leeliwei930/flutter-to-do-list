import 'package:flutter/material.dart';
import '../util/Task.dart';
import '../util/Auth.dart';
class ListAllTask extends StatefulWidget {

  final Task task;
  final Auth auth;
  final BuildContext scaffoldContext;
  ListAllTask(this.task,this.auth, this.scaffoldContext);
  ListAllTaskState createState() => new ListAllTaskState();
}

class ListAllTaskState extends State<ListAllTask> {

  void initState() {
    // TODO: implement initState
    super.initState();
    widget.task.getAllTask(this);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ListView.builder(
        itemBuilder: (context, int index) {
          return new Container(
            child: new ListTile(
              leading:   new Checkbox(

                  value:  widget.task.taskList[index]["finished"] , onChanged: (val)async{
                await widget.task.markTask( widget.task.taskList[index] , this , context);


              }),
                onTap: () {
                null;
                },
                title: new Padding(
                    padding: new EdgeInsets.only(top: 12.0),

                    child: new Text(
                      widget.task.taskList[index]['task_name'].toString(),
                      overflow: TextOverflow.ellipsis,

                      style: new TextStyle(fontSize: 20.00 ,  ),
                    )),
                subtitle: new Padding(
                  padding: new EdgeInsets.symmetric(vertical: 12.0),
                  child: new Text(
                    widget.task.taskList[index]['task_description'],
                    overflow: TextOverflow.ellipsis,

                    style: new TextStyle(fontSize: 16.00),

                  ),
                ),
                trailing: new Row(
                  children: <Widget>[


                    new IconButton(
                      icon: new Icon(Icons.delete),
                      onPressed: ()async {
                          await widget.task.deleteTask(widget.task.taskList[index]['id'].toString() , context,this);


                          },
                    ),
                  ],

                )

            ),
              decoration:
              new BoxDecoration(
                  border: new  Border(
                    bottom: new BorderSide(color: Colors.grey, width: 0.30 )
                  )
              )
                    );
        },
        itemCount: widget.task.taskList.length == null
            ? 0
            : widget.task.taskList.length);
  }
}
