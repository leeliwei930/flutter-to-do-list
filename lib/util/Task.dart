import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:flutter/material.dart';

class Task {
  List taskList = [];

  void getAllTask(state) async {
//    var http = createHttpClient();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await http
        .get(Uri.parse(prefs.getString("endpoint") + '/api/task'), headers: {
      'Authorization': 'Bearer ' + prefs.getString('access_token'),
      'Accept': "application/json",
      'X-Requested-with': "XMLHttpRequest",
    }).then((response) {
      state.setState(() {
        this.taskList = json.decode(response.body);
      });
    }).catchError((errors) {
      state.setState(() {
        print(
            "Unable to fetch the data, Please check for your internet connection");
      });
    });
  }
//
//    http.get(Uri.decodeFull(url)).then((response) {
//      print(response);
//    }).catchError((errors) {
//      print(errors);
//    });

  Future<bool> createTask(
      String taskName, String taskDescription, state) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Object params = {
      "task_name": taskName.toString(),
      "task_description": taskDescription.toString(),
      "finished": false
    };
    taskList = this.taskList;
    Object taskListAppendParams;
    bool created = false;
    await http.post(prefs.getString('endpoint') + "/api/task/create",
        body: json.encoder.convert(params),
        headers: {
          'Authorization': "Bearer " + prefs.getString('access_token'),
          'X-Requested-With': "XMLHttpRequest",
          'Content-Type': "application/json",
          'Accept': "application/json"
        }).then((response) {
      var decodedResponse = json.decode(response.body);
      taskListAppendParams = {
        "id" : decodedResponse['id'].toString(),
        "task_name": decodedResponse['task_name'].toString(),
        "task_description": decodedResponse['task_description'].toString(),
        "finished": false
      };
      created = true;
    }).catchError((errors) {
      print(errors);
      created = false;
    });

    if (created) {
      state.setState(() {
        taskList.add(taskListAppendParams);
      });
    }
    return created;
  }

//  void deleteTask(int id)async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    this.taskList.removeWhere((task) => task["id"] == id.toInt());
//  }

  Future<void> markTask(task, state, context) async {
//    this.taskList[index]["finished"] = true;
    BuildContext parentContext = context;
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    print(task['id']);
    await http.patch(
        prefs.getString('endpoint') +
            '/api/task/' +
            task['id'].toString() +
            '/mark',
        headers: {
          'Authorization': 'Bearer ' + prefs.getString('access_token'),
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'X-Requested-With': "XMLHttpRequest"
        }).then((response) {
      print(response.body);
      state.setState(() {
        if (response.statusCode.toString() == "200") {
          var decodedResponseBody = json.decoder.convert(response.body);
          task["finished"] = decodedResponseBody["updatedTaskState"];

          Scaffold.of(parentContext).showSnackBar(new SnackBar(
                content: new Padding(
                    padding: new EdgeInsets.all(5.00),
                    child: new Row(
                      children: <Widget>[
                        new Icon(Icons.check_circle),
                        new Text(
                          ' Task mark as ' +
                              ((task['finished'])
                                  ? "Completed"
                                  : "Uncompleted"),
                          style: new TextStyle(fontSize: 14.00),
                        ),
                      ],
                    )),
                duration: new Duration(milliseconds: 550),
              ));
        }
      });
    }).catchError((errors) {
      print(errors);
    });
  }

  Future<bool> deleteTask(String taskID,context,state) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    BuildContext parentContext = context;
    bool isDeleted = false;
    await http.delete(
        Uri.parse(
            preferences.getString('endpoint') + "/api/task/$taskID/delete"),
        headers: {
          'Authorization' : "Bearer "+preferences.getString('access_token'),
          'Content-Type' : "application/json",
          'Accept' : "application/json",
          'X-Requested-With' : "XMLHttpRequest"
        },
        )
        .then((response){

      if(response.statusCode.toString() == "200"){
        this.taskList.removeWhere((task) => task["id"].toString() == taskID.toString());

        var decodedJSONResponse = json.decoder.convert(response.body);
            state.setState(() {

            if(decodedJSONResponse['status'].toString() == "true"){

                Scaffold.of(parentContext).showSnackBar(new SnackBar(
                  content: new Padding(
                      padding: new EdgeInsets.all(5.00),
                      child: new Row(
                        children: <Widget>[
                          new Icon(Icons.delete),
                          new Text(
                            " Task Deleted Successfully",
                            style: new TextStyle(fontSize: 14.00),
                          ),
                        ],
                      )),
                  duration: new Duration(seconds: 3),
                ));
              isDeleted =  true;

            } else {
              isDeleted = false;
            }
            });

      }
        })
        .catchError((errors){
          print(errors);
          isDeleted = false;
        });
      return isDeleted;
  }
}
