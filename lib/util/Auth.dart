import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

class Auth {
  String clientID = "2";
  String clientSecret = "lAzfkL564MPBxLkHQaAhJ3u9g2q1uv6Rrp1SASPS";
  String accessToken = "";
  String refreshToken = "";
  String name = "";
  String email = "";
  String tokenExpiredDateTime = "";
  bool fileExists = false;
  DateTime currentDateTime = new DateTime.now();

  Auth(){

    setUserInfo();
  }

  void setUserInfo()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    this.email = prefs.getString('email');
    this.name = prefs.getString('name');
  }
  Future<bool> getToken(String username, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Object params = {
      "client_id": this.clientID.toString(),
      "client_secret": this.clientSecret.toString(),
      "grant_type": "password",
      "username": username,
      "password": password
    };
    print(json.encoder.convert(params));
    bool userCredentialsVerified;

    await http
        .post(Uri.parse(prefs.getString('endpoint') + '/oauth/token'),
            headers: {"Content-Type": "application/json"},
            body: json.encoder.convert(params))
        .then((response) {
      print(response.statusCode.toString());

      var jsonResponse = json.decode(response.body);
      this.accessToken = jsonResponse['access_token'];
      this.refreshToken = jsonResponse['refresh_token'];

      this.tokenExpiredDateTime = new DateTime.now()
          .add(new Duration(seconds: jsonResponse['expires_in']))
          .toString();
      this.verifyToken();

      if (response.statusCode == 200){

        userCredentialsVerified = true;
      } else {
        userCredentialsVerified = false;
      }
    }).catchError((errors) {
      print(errors);
      userCredentialsVerified = false;
    });
    return userCredentialsVerified;
  }

  Future<bool> refreshNewToken() async {
    bool tokenRefreshSuccess;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Object params = {
      "client_id": this.clientID.toString(),
      "client_secret": this.clientSecret.toString(),
      "grant_type": "refresh_token",
      "refresh_token": prefs.get('refresh_token').toString()
    };
    await http
        .post(Uri.parse(prefs.getString('endpoint') + '/oauth/token'),
            headers: {'Content-Type': 'application/json'},
            body: json.encoder.convert(params))
        .then((response) {
      var jsonResponse = json.decode(response.body);
      this.accessToken = jsonResponse['access_token'];
      this.refreshToken = jsonResponse['refresh_token'];
      this.tokenExpiredDateTime = jsonResponse['expires_in'];

      prefs.setString('access_token', this.accessToken);
      prefs.setString('refresh_token', this.refreshToken);
      prefs.setString(
          'token_life_time',
          new DateTime.now()
              .add(new Duration(seconds: jsonResponse['expires_in']))
              .toString());
      if (response.statusCode == 200) {
        tokenRefreshSuccess = true;
      } else {
        tokenRefreshSuccess = false;
      }
    }).catchError((errors) {
      tokenRefreshSuccess = false;
    });

    return tokenRefreshSuccess;
  }

  Future<bool> verifyToken() async {
    bool isSignedIn = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await http
        .get(Uri.parse(prefs.getString('endpoint').toString() + '/api/user'), headers: {
      "X-Requested-With": "XMLHttpRequest",
      'Accept': "application/json",
      'Authorization': "Bearer " + prefs.getString('access_token').toString()
    }).then((response) {
      if (response.statusCode.toString() == "200") {
        var decodedJsonResponse = json.decoder.convert(response.body);
        prefs.setString('name', decodedJsonResponse['name']);
        prefs.setString('email', decodedJsonResponse['email']);

        this.email =  prefs.getString('email');
        this.name = prefs.getString('name');
        isSignedIn = true;

      }
      if (response.statusCode.toString() == "401") {
        isSignedIn = false;
      }
    }).catchError((errors) {
      isSignedIn = false;
    });
    return isSignedIn;
  }

  Future<Object> getCurrentUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Object userInfo = {
      'name': this.getCurrentUserName(),
      'email': this.getCurrentUserEmail()
    };
    return userInfo;
  }

  String getCurrentUserName() {
    return this.name;
  }

  String getCurrentUserEmail() {
    return this.email;
  }

  Future <bool> logout(context,auth) async {
    BuildContext parentContext = context;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedOut = false;
    await http
        .get(prefs.getString('endpoint') + '/api/user/logout', headers: {
        'Authorization' : "Bearer " + prefs.getString('access_token'),
        'Accept' : "application/json",
        'Content-Type' : "application/json",
        'X-Requested-with'  : "XMLHttpRequest"
        })
        .then((response) {
          print(response.body);
          if(response.statusCode.toString() == "200"){
            var decodedJSONresponse = json.decoder.convert(response.body);

            if(decodedJSONresponse['logout'].toString() == "true"){
              prefs.setString('access_token', null);
              prefs.setString('refresh_token', null);
              prefs.setString('tokenExpiredDateTime', null);
              isLoggedOut = true;
              auth = new Auth();

            } else {
              isLoggedOut  = false;
            }
            Scaffold.of(parentContext).showSnackBar( new SnackBar(



              content: new Padding(padding: new EdgeInsets.all(5.00) , child: new Row(children: <Widget>[
                new Icon((isLoggedOut) ? Icons.check_circle : Icons.warning),
                new Text(' Task mark as ' + ((isLoggedOut) ? "Logged Out Successfully..." : decodedJSONresponse['message']) , style: new TextStyle(fontSize: 14.00),),
              ],)),
              duration: new Duration(seconds: 3),
            ));
          }

        })
        .catchError((errors) {
          print(errors);
    });
   return isLoggedOut = true;

  }
  Future <bool> login(loginFormKey,username,password,approval) async {

    var form = loginFormKey.currentState;

    bool signInApproved = await this.getToken(username.toString(), password.toString());
    approval.signInApproved = signInApproved;
    if (form.validate()) {
      SharedPreferences prefs = await SharedPreferences.getInstance();


      prefs.setString('access_token' , this.accessToken);
      prefs.setString('refresh_token' , this.refreshToken);
      prefs.setString('token_life_time',  this.tokenExpiredDateTime);
      prefs.setString('name' , this.name);
      prefs.setString('email', this.email);
       signInApproved = true;
    } else {
      signInApproved =  false;
    }
    approval.signInApproved = signInApproved;
    return signInApproved;
  }
  void clearToken(){

  }
}
