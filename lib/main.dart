import 'package:flutter/material.dart';
import './page/createTaskPage.dart';
import './util/Task.dart';
import './page/Login.dart';
import './page/ListAllTask.dart';
import './page/ListSelectedTask.dart';
import './page/SidebarMenu.dart';
import './util/Auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:core';

Task task = new Task();
Auth auth = new Auth();


void main() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('endpoint', "http://a3927372.ngrok.io");


  bool userHasLoggedIn = false;
  DateTime currentDateTime = new DateTime.now();

  if (prefs.getString('access_token') == null ||
      prefs.get("refresh_token") == null ||
      prefs.get('token_life_time') == null) {
    userHasLoggedIn = false;
  } else {
    if (currentDateTime
        .isAfter(DateTime.parse(prefs.get('token_life_time').toString()))) {
//      print("token expired");
      await auth.refreshNewToken();
      await auth.verifyToken();
      userHasLoggedIn = false;
    } else {
//      print("token not expired");

      userHasLoggedIn = await auth.verifyToken();
    }
  }

  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
      home: (!userHasLoggedIn) ? new Login(prefs,auth) : new MainView(),
      routes: <String, WidgetBuilder>{
        '/main': (BuildContext context) => new MainView(),
        '/login' : (BuildContext context) => new Login(prefs,auth),
        '/createtask': (BuildContext context) => new CreateTaskPage(task)
      }));
}

class MainView extends StatefulWidget {
  MainViewState createState() => new MainViewState();

}

class MainViewState extends State<MainView> with TickerProviderStateMixin {
  TabController mainViewTabController;
  BuildContext scaffoldContext;
  @override
  void initState() {
    super.initState();
    this.setState((){

      mainViewTabController = new TabController(length: 23, vsync: this);
    });
  }

  Widget build(BuildContext context) {
    // TODO: implement build

    return new Scaffold(
        drawer: new SidebarMenu(auth,scaffoldContext,this),
        appBar: new AppBar(
          title: new Text("To Do List"),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.add),
              onPressed: () => Navigator.of(context).pushNamed('/createtask'),
              color: Colors.white,
            )
          ],
          backgroundColor: Colors.deepOrange,
          bottom: new TabBar(controller: mainViewTabController, tabs: [
            new Tab(text: "ALL"),
            new Tab(text: "FINISHED"),
            new Tab(text: "UNFINISHED")
          ]),
        ),
        body: new TabBarView(controller: mainViewTabController, children: [
          new Container(
            child: new ListAllTask(task, auth, scaffoldContext),
          ),
          new Container(
              child: new Center(
            child: new Column(
              children: <Widget>[
                new RaisedButton(child: new Text("Get Token"), onPressed: () {})
              ],
            ),
          )),
          new Container()
        ]));
  }
}
